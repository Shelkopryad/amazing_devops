package amazing_calc_tests;

import amazing_calc.Calc;
import io.qameta.allure.Step;
import org.junit.Assert;
import org.junit.Test;

public class CalcTest {

    @Test
    @Step("Try to sum 45 and 44")
    public void sumTest() {
        Assert.assertEquals("Not equals!", 89, new Calc().sum(45, 44));
    }

    @Test
    @Step("Try to multiply 5 and 6")
    public void multiplyTest() {
        Assert.assertEquals("Not equals!", 30, new Calc().multiply(5, 6));
    }

    @Test
    @Step("Try to deduct 18 and 13")
    public void deductTest() {
        Assert.assertEquals("Not equals!", 5, new Calc().deduct(18, 13));
    }

    @Test
    @Step("Try to divide 16 and 4")
    public void divideTest() {
        Assert.assertEquals("Not equals!", 4, new Calc().divide(16, 4));
    }

    @Test
    @Step("Try to divide by zero")
    public void divideByZeroTest() {
        Assert.assertEquals("Not equals!", Integer.MAX_VALUE, new Calc().divide(84, 0));
    }

    @Test
    @Step("Test square 5")
    public void squreTest() {
        Assert.assertEquals("Not equals!", 25, new Calc().square(5));
    }

}
